import moment from 'moment'

export const note = state => {
    return state.note
}

export const notes = state => {

    return state.notes.sort(function (a, b) {
        if (a.lastSaved < b.lastSaved) {
            return 1;
        }
        if (a.lastSaved > b.lastSaved) {
            return -1;
        }
        // a must be equal to b
        return 0;
    });
}

export const lastSaved = state => {
    if(!state.note.lastSaved) {
        return 'Never'
    }

    return moment(state.note.lastSaved).calendar()
}

export const wordCount = state => {
    if(!state.note.body || state.note.body.trim() === '') {
        return 0
    }

    return state.note.body.trim().split(' ').length
}